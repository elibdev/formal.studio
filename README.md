# Formal Studio

Formal Studio is an open source website creation platform designed to serve the unique needs of artists and creative professionals.

You can view past and upcoming changes in [the changelog.](CHANGELOG.md)

## Installing

[Install the Elixir programming language.](https://elixir-lang.org/install.html)


Install the Elixir dependencies:

```
mix deps.get
```

Run the tests to make sure everything's working:

```
mix test
```

## Running the Server

You can run the server as a long-running process using this command:

```
mix run --no-halt
```

You can an interactive Elixir shell for the application using this command:

```
iex -S mix
```

## Generating the Documentation

To generate local documentation for the code, run this command:

```
mix docs
```

