# A Modern Website Creation Platform - Announcing Formal Studio Beta Preview

_Note: The Formal Studio Website Builder is no longer live due to lack of sustained usage._

For the past year give-or-take, I've been working on a modern open source platform for creating websites. Today I'm announcing the beta preview of Formal Studio. 

If you just want to try it out, you can [sign up and create a free website now.](https://formal.studio) It's still rough around the edges, but the basic functionality is there and I just can't wait to get it out the door. I want to work with the broader community to plan the development roadmap moving forward, so please feel free to comment on this post or write me directly at [eli@formal.studio](mailto:eli@formal.studio) with any questions or suggestions. 

## Creative People Are Not Food For Tech Companies

It feels like too many tech companies view creative people as a resource to mine for ad revenue. They use our inability to run our own platfoms to funnel us into their "free" white-washed services, while selling ad space next to our content and keeping all the profits. 

When they look at the content that creative people like you worked hard to create, they only see click-bait and advertising dollars. Platforms like Facebook, Instagram, and Twitter limit how you can express yourself and who you can reach, and they censor legitimate content to make their platforms more advertiser-friendly. At the end of the day you are just a metric to them. 

## I Built Formal Studio to Empower Creative People

Creatively expressing yourself through your own indepedent space on the web is still not as accessible as I think it should be. My hope for Formal Studio is that it can grow to be an open source option that is super simple to use and customize. 

Here's how I see people currently building websites based on their budget:

- **$0:** create a free ad-supported website or just use social media
- **$100:** build a website using one of Squarespace's proprietary themes
- **$1,000:** hire a developer to tweak a Wordpress theme
- **$10,000:** hire a developer to build a totally custom Wordpress theme

See any funny patterns? Creative freedom costs big bucks. I think most website creation platforms today are designed to serve the needs of businesses and marketing campaigns. 

## Didn't Anyone Tell Them That Artists Are Broke?

I'm building Formal Studio for the artists and creative professionals that don't have big marketing budgets and are under-served by the current platforms. 

Most people that come to me saying they need a new website are just looking to share the fruits of their creative labor with the world and see what happens. 

Formal Studio is my best attempt to make it super easy for creative people to start small and end big. You can build a website that you can fully customize for free, and if you ever you need to grow past that you can set up your own installation or upgrade to a premium hosting plan (coming soon). 

I'd love for you to [try it out and create a free website.](https://formal.studio)

## Some Technical Details For The Techies

- I'm releasing Formal Studio under an MIT License.
- You can access the source code at [code.formal.studio](https://code.formal.studio). 
- You write pages using the text formatting standard Markdown.
- You can customize your website's style using CSS.
- The software is written in Elixir, a fast a friendly modern language. 
- If you want to get involved, just email me at [eli@formal.studio](mailto:eli@formal.studio).

I'm planning on writing a bunch of technical articles about the process of building Formal Studio in Elixir. If there's anything you'd like to read about, please comment and let me know!
