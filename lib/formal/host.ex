defmodule Formal.Host do
  @moduledoc """
  A `Formal.Web.Service` that serves multiple websites
  from SQLite databases in a single directory.

  This module maintains a database of users and which sites they own.

  You can optionally provide a keyword arguments with the keys 
  `:port` and `:dir` to specify the port to listen on
  and the directory to serve the database files from,
  and `:insecure` to not enforce HTTPS.

  You can set the following environment variables to override start args:

  - `STUDIO_PORT` (default 8000): the port to serve on
  - `STUDIO_DIR` (default `/var/lib/formal/`): the directory to serve from
  - `STUDIO_INSECURE` (default `false`): set to `true` to not require HTTPS
  """
  import Comeonin.Argon2
  alias Formal.Web
  alias Formal.Host.Pages
  use Web.Service

  @impl GenServer
  def init(args) do
    port = env_port() || args[:port] || 8000
    dir = env_dir() || args[:dir] || "/var/lib/formal/"
    insecure = env_insecure() || args[:insecure] || false

    site_dir = Path.join(dir, "sites")
    File.mkdir_p!(site_dir)

    host_db = open(Path.join(dir, "host"))

    Formal.Site.Supervisor.start_link()
    # run a service for each existing site
    Enum.each(File.ls!(site_dir), fn name ->
      path = Path.join(site_dir, name)
      {:ok, _} = Formal.Site.Supervisor.start_child(path)
    end)

    state = %{
      host_db: host_db,
      site_dir: site_dir,
      insecure: insecure
    }

    :elli.start_link(
      port: port,
      callback: Formal.Host,
      callback_args: state
    )
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  defp env_port() do
    with port when not is_nil(port) <- System.get_env("STUDIO_PORT"),
         do: String.to_integer(port)
  end

  defp env_dir() do
    System.get_env("STUDIO_DIR")
  end

  defp env_insecure() do
    case System.get_env("STUDIO_INSECURE") do
      "true" -> true
      _ -> false
    end
  end

  # Open the database holding the host data at the given path,
  # creating it if it doesn't exist yet.
  defp open(path) do
    db =
      case File.exists?(path) do
        true ->
          {:ok, db} = Depo.open(path)
          db

        false ->
          {:ok, db} = Depo.open(create: path)
          db
      end

    schema = """
    create table if not exists users (
      id integer primary key,
      email unique, 
      password_hash
    );
    create table if not exists keys (
      key primary key,
      user references users
    );
    create table if not exists sites (
      name unique primary key,
      user references users
    )
    """

    Depo.write(db, schema)

    Depo.teach(db, %{
      join: "insert into users (email, password_hash) values (?1, ?2)",
      user_from_email: "select id,password_hash from users where email=?1",
      new_key: "insert into keys (user, key)
      values ((select id from users where email=?1), ?2)",
      user_keys: "select key from keys 
      where user=(select id from users where email=?1)",
      user_from_key: "select email,id from users
      where id=(select user from keys where key=?1)",
      remove_key: "delete from keys where key=?1",
      new_site: "insert into sites values 
      (?1, (select id from users where id=(select user from keys where key=?2)))",
      sites_for_key: "select name from sites 
      where user=(select user from keys where key=?1)"
    })

    db
  end

  @doc """
  Get the a list of sites that a user owns from their key.

  Returns `nil` if the user doesn't exist, or an empty list if they have no sites.
  """
  def sites_from_key(db, key) do
    cond do
      Depo.read(db, :user_from_key, key) == [] ->
        nil

      true ->
        Depo.read(db, :sites_for_key, key)
        |> Enum.map(fn %{name: name} -> name end)
    end
  end

  @doc """
  Create a new user with the given email.

  Returns `{key, pass}` with an access key and passphrase for the user.
  """
  def join(host_db, email) do
    # create a new account for the user's new site
    pass = Passphrase.new(4)
    hash = hashpwsalt(pass)
    key = Web.random_string(64)

    Depo.transact(host_db, fn ->
      # make sure email is not already used
      [] = Depo.read(host_db, :user_from_email, email)
      Depo.write(host_db, :join, [email, hash])
      Depo.write(host_db, :new_key, [email, key])
    end)

    {key, pass}
  end

  @doc """
  Log in a user with with given email and passphrase.

  Returns an access key on success, or `nil` on failure.
  """
  def login(host_db, email, pass) do
    # running a dummy check prevents user enumeration via timing info
    valid =
      case Depo.read(host_db, :user_from_email, email) do
        [] -> dummy_checkpw()
        [%{password_hash: hash}] -> checkpw(pass, hash)
      end

    case valid do
      false ->
        nil

      true ->
        # create a new login key for the user
        key = Web.random_string(64)
        Depo.write(host_db, :new_key, [email, key])
        key
    end
  end

  @doc """
  Logout the given access key, removing it from the database.
  """
  def logout(host_db, key) do
    Depo.write(host_db, :remove_key, key)
  end

  @doc """
  Create a new site in the given directory owned by the given user key
  with the given name using the markdown as the home page.
  """
  def new_site(host_db, site_dir, key, name, content) do
    # the site name can only include alphanumeric characters and dashes
    false = Enum.any?(to_charlist(name), &URI.char_reserved?/1)

    Depo.transact(host_db, fn ->
      Depo.write(host_db, :new_site, [name, key])

      site = %{
        "name" => URI.encode(name),
        "content" => content
      }
      
      Path.join(site_dir, name)
      |> Formal.Site.Supervisor.start_child()
      "/#{site["name"]}"
    end)
  end

  @impl Web.Service
  def route(:GET, [], headers, _, state) do
    %{host_db: host_db} = state

    key =
      case headers do
        %{"cookie" => %{"key" => key}} -> key
        _ -> throw({200, [], Pages.join()})
      end

    case sites_from_key(host_db, key) do
      nil ->
        {200, [], Pages.join()}

      [] ->
        {302, [{"location", "/?new-site"}], ""}

      [name] ->
        pages = Formal.Site.call(name, :pages)
        {200, [], Pages.site(name, pages)}
    end
  end

  def route(:GET, ["style.css"], _, _, _) do
    style = File.read!(Path.join(:code.priv_dir(:formal), "style.css"))
    {200, [{"content-type", "text/css"}], style}
  end

  def route(:GET, {[], %{"login" => _}}, _, _, _) do
    {200, [], Pages.login()}
  end

  def route(:GET, {[], %{"new-page" => _}}, headers, _, %{host_db: host_db}) do
    %{"cookie" => %{"key" => key}} = headers
    true = is_list(sites_from_key(host_db, key))

    {200, [], Formal.Site.Pages.new_page()}
  end

  def route(:GET, {[], %{"new-site" => _}}, headers, _, %{host_db: host_db}) do
    %{"cookie" => %{"key" => key}} = headers
    # each account should only have one site
    [] = sites_from_key(host_db, key)

    {200, [], Pages.new_site()}
  end

  def route(:GET, [name], headers, _, %{host_db: host_db}) do
    page = Formal.Site.call(name, :page, "")

    page =
      with key when is_binary(key) <- headers["cookie"]["key"],
           sites when is_list(sites) <- sites_from_key(host_db, key),
           name in sites do
        Map.put(page, :nav, [%{href: "?edit", text: "Edit this page."}])
      else
        _ -> page
      end

    {200, [], Formal.Site.Pages.page(page, name)}
  end

  def route(:GET, {[name], %{"edit" => _}}, headers, _, state) do
    %{host_db: host_db} = state
    %{"cookie" => %{"key" => key}} = headers
    true = name in sites_from_key(host_db, key)
    markdown = Formal.Site.call(name, :page_markdown, "")

    {200, [], Formal.Site.Pages.edit_home(name, markdown)}
  end

  def route(:GET, [name, "style.css"], _, _, _) do
    style = Formal.Site.call(name, :style)
    {200, [{"content-type", "text/css"}], style}
  end

  def route(:GET, [site_name, page_name], headers, _, state) do
    %{host_db: host_db} = state

    page = Formal.Site.call(site_name, :page, page_name)
    %{title: site_title} = Formal.Site.call(site_name, :page, "")
    page = Map.put(page, :site_title, site_title)

    page =
      with key when is_binary(key) <- headers["cookie"]["key"],
           sites when is_list(sites) <- sites_from_key(host_db, key),
           site_name in sites do
        Map.put(page, :nav, [%{href: "?edit", text: "Edit this page."}])
      else
        _ -> page
      end

    {200, [], Formal.Site.Pages.page(page, site_name)}
  end

  def route(:GET, {[site_name, "style.css"], %{"edit" => _}}, headers, _, state) do
    %{host_db: host_db} = state
    %{"cookie" => %{"key" => key}} = headers
    true = site_name in sites_from_key(host_db, key)
    style = Formal.Site.call(site_name, :style)

    {200, [], Formal.Site.Pages.edit_page("style.css", style)}
  end

  def route(:GET, {[site_name, page_name], %{"edit" => _}}, headers, _, state) do
    %{host_db: host_db} = state
    %{"cookie" => %{"key" => key}} = headers
    true = site_name in sites_from_key(host_db, key)
    markdown = Formal.Site.call(site_name, :page_markdown, page_name)

    {200, [], Formal.Site.Pages.edit_page(page_name, markdown)}
  end

  def route(:POST, [], _, body, state) do
    %{host_db: host_db} = state
    %{"email" => email} = body

    {key, pass} = join(host_db, email)

    headers = [
      Web.login_cookie(key, state[:insecure]),
      {"cache-control", "no-cache,no-store,must-revalidate"}
    ]

    {201, headers, Pages.passphrase(pass)}
  end

  def route(:POST, {[], %{"new-page" => _}}, headers, body, state) do
    %{host_db: host_db} = state
    %{"cookie" => %{"key" => key}} = headers
    %{"name" => name, "content" => content} = body

    [site_name] = sites_from_key(host_db, key)
    Formal.Site.call(site_name, :edit_page, [name, content])

    {302, [{"location", "/#{site_name}/#{name}"}], ""}
  end

  def route(:POST, {[], %{"new-site" => _}}, headers, body, state) do
    %{host_db: host_db, site_dir: site_dir} = state
    %{"cookie" => %{"key" => key}} = headers
    %{"name" => name, "content" => content} = body

    # each account should only be able to have one site
    [] = sites_from_key(host_db, key)
    url = new_site(host_db, site_dir, key, name, content)

    {302, [{"location", url}], ""}
  end

  def route(:POST, {[], %{"login" => _}}, _, body, state) do
    %{"email" => email, "passphrase" => pass} = body
    %{host_db: host_db} = state

    case login(host_db, email, pass) do
      nil ->
        {403, [], "That login is invalid."}

      key ->
        {302,
         [
           Web.login_cookie(key, state[:insecure]),
           {"location", "/"}
         ], ""}
    end
  end

  def route(:POST, {[], %{"logout" => _}}, headers, _, state) do
    %{"cookie" => %{"key" => key}} = headers
    %{host_db: host_db} = state
    logout(host_db, key)
    {302, [{"location", "/"}], ""}
  end

  def route(:POST, {[name], %{"edit" => _}}, headers, body, state) do
    %{host_db: host_db} = state
    %{"cookie" => %{"key" => key}} = headers
    %{"content" => content} = body

    names = sites_from_key(host_db, key)
    true = name in names
    Formal.Site.call(name, :edit_page, ["", content])

    {302, [{"location", "/#{name}"}], ""}
  end

  def route(:POST, {[name, "style.css"], %{"edit" => _}}, headers, body, state) do
    %{host_db: host_db} = state
    %{"cookie" => %{"key" => key}} = headers
    %{"content" => content} = body

    names = sites_from_key(host_db, key)
    true = name in names
    Formal.Site.call(name, :edit_style, content)

    {302, [{"location", "/"}], ""}
  end

  def route(:POST, {[name, page], %{"edit" => _}}, headers, body, state) do
    %{host_db: host_db} = state
    %{"cookie" => %{"key" => key}} = headers
    %{"content" => content} = body

    names = sites_from_key(host_db, key)
    true = name in names
    Formal.Site.call(name, :edit_page, [page, content])

    {302, [{"location", "/#{name}/#{page}"}], ""}
  end
end
