defmodule Formal.Web.Requester do
  @moduledoc """
  This module is `Tesla.Middleware` that returns a response tuple
  instead of the normal response map.

  It returns a response in the form of `{status, headers, body}`
  where `headers` is a map.
  """
  @behaviour Tesla.Middleware

  def call(env, next, _options) do
    {:ok, resp} = Tesla.run(env, next)
    headers = decode_headers(resp.headers)

    {resp.status, headers, resp.body}
  end

  def decode_headers(headers) do
    Enum.map(headers, &decode_header/1)
    |> Enum.into(%{})
  end

  defp decode_header({key, value}) do
    value =
      value
      |> String.split([";", ","])
      |> Enum.map(&String.trim/1)
      |> Enum.map(&URI.decode_query/1)
      |> Enum.reduce(&Map.merge/2)

    {key, value}
  end
end
