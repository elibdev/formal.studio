defmodule Formal.Web.Components do
  @moduledoc """
  This module generates Elixir functions from EEx templates
  in the `ui` folder in the `:formal` app's priv directory.

  To use the macro in this module you must `use` it, for example:

  ```
  defmodule UI do
    use Formal.Web.Components
  end
  ```
  """

  @doc """
  """
  defmacro __using__(_) do
    quote bind_quoted: [] do
      require EEx
      ui = :code.priv_dir(:formal) |> Path.join("ui")
      templates = File.ls!(ui)

      Enum.each(templates, fn template ->
        name = Path.rootname(Path.basename(template))
        ext = Path.extname(template)
        file = Path.join(ui, template)
        function = String.to_atom(String.replace(name, "-", "_"))

        if ext == ".eex" do
          EEx.function_from_file(:def, function, file, [:assigns])
        end
      end)
    end
  end
end
