defmodule Formal.Web.UI do
  @moduledoc """
  Modular HTML components for building pages. 
  """
  use Formal.Web.Components
end
