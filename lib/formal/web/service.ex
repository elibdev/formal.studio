defmodule Formal.Web.Service do
  @moduledoc """
  You can `use` this module to create a web server gateway.

  This is a stateless external interface to internal services.
  """

  @type string_map :: %{optional(String.t()) => String.t()}
  @type path :: [String.t()] | {[String.t()], string_map}
  @type method :: :GET | :POST | :PUT | :DELETE | :PATCH | :HEAD | :CONNECT | :OPTIONS | :TRACE

  @callback route(
              method :: method,
              path :: path,
              headers :: string_map,
              body :: string_map,
              state :: any
            ) :: any

  defmacro __using__(_) do
    quote bind_quoted: [] do
      use GenServer
      @behaviour :elli_handler
      @behaviour Formal.Web.Service

      defp via(name) do
        {:via, Registry, {Formal.Registry, {__MODULE__, name}}}
      end

      def run(name, args) do
        GenServer.start_link(__MODULE__, args, name: via(name))
      end

      def whereis(name) do
        GenServer.whereis(via(name))
      end

      def call(name, func, args \\ [])

      def call(name, func, args) when is_atom(func) and is_list(args) do
        GenServer.call(via(name), {func, args})
      end

      def call(name, func, arg) when is_atom(func) do
        GenServer.call(via(name), {func, [arg]})
      end

      @impl GenServer
      def handle_call({func, args}, _from, state) when is_atom(func) and is_list(args) do
        reply = apply(__MODULE__, func, [state | args])
        {:reply, reply, state}
      end

      def start(_type, args) do
        children = [
          {Registry, keys: :unique, name: Formal.Registry},
          {__MODULE__, args}
        ]

        opts = [strategy: :one_for_one, name: Formal.Supervisor]
        Supervisor.start_link(children, opts)
      end

      @impl :elli_handler
      def handle_event(_event, _data, _args) do
        :ok
      end

      @impl :elli_handler
      def handle(request, state) do
        path = :elli_request.path(request)
        method = :elli_request.method(request)
        query = :elli_request.get_args(request) |> Enum.into(%{})

        path =
          case query do
            query when query == %{} -> path
            query -> {path, query}
          end

        body =
          :elli_request.body(request)
          |> URI.decode_query()

        headers =
          :elli_request.headers(request)
          |> Enum.map(fn {key, value} -> {String.downcase(key), value} end)
          |> Formal.Web.Requester.decode_headers()
          |> Enum.into(%{})

        apply(__MODULE__, :route, [method, path, headers, body, state])
      end
    end
  end
end
