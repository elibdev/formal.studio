defmodule Formal.Site.Pages do
  alias Formal.Web.UI
  def new_page do
    UI.admin(
      title: "Create a New Page",
      description: "Create a new page.",
      body: UI.new_page([])
    )
  end

  def page(page, name) do
    page = Map.put(page, :name, name)
    UI.page(page)
  end

  def site(name, pages) do
    UI.admin(
      title: "Welcome to Your Site #{name}",
      description: "Manage your site's pages.",
      body: UI.site(name: name, pages: pages),
      nav: [%{href: "?new-page", text: "Create a new page."}]
    )
  end

  def edit_home(name, markdown) do
    UI.admin(
      title: "Edit the Home Page of #{name}",
      description: "",
      body: UI.edit_home(name: name, markdown: markdown)
    )
  end

  def edit_page(name, markdown) do
    UI.admin(
      title: "Edit Page #{name}",
      description: "",
      body: UI.edit_page(name: name, markdown: markdown)
    )
  end
end
