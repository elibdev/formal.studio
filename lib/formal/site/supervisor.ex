defmodule Formal.Site.Supervisor do
  use DynamicSupervisor

  def start_link() do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @impl DynamicSupervisor
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_child(path) do
    name = {:via, Registry, {Formal.Registry, {Formal.Site, Path.basename(path)}}}
    spec = {Formal.Site, [path: path, name: name]}
    {:ok, _} = DynamicSupervisor.start_child(__MODULE__, spec)
  end
end
