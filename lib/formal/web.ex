defmodule Formal.Web do
  @doc """
  Get a client that makes requests to `localhost` with the given port.

  You can pass this client into the request functions in this module.
  """
  def client(port) do
    Tesla.build_client([
      {Tesla.Middleware.BaseUrl, "http://localhost:#{port}"},
      {Tesla.Middleware.FormUrlencoded, []},
      {Formal.Web.Requester, []}
    ])
  end

  @doc """
  Make an HTTP GET request using the given client to the given path
  with the optional given options. 
  The options map can include `:headers` or `:body`.

  Returns a tuple `{status, headers, body}` where headers is a map
  and body is a string.
  """
  def get(client, path, opts \\ []) do
    Tesla.get(client, path, opts)
  end

  @doc """
  Make an HTTP POST request using the client to the given path with the given body.

  Uses the same format as `get/3` for the options and the response.
  """
  def post(client, path, body, opts \\ []) do
    Tesla.post(client, path, body, opts)
  end

  @doc """
  Get the first text from an HTML document of a single element
  that matches the given CSS selector.

  Returns `true` if the element is empty, contains other elements, or
  if there are multiple matches.
  Returns `false` if no elements match the selector.
  """
  def select(html, selector) do
    case Floki.find(html, selector) do
      [{_, _, [text]} | _] when is_binary(text) -> text
      [{_, _, elements} | _] when is_list(elements) -> true
      [] -> false
    end
  end

  @doc """
  Get a random URL-safe string of the given length.
  """
  def random_string(length) do
    :crypto.strong_rand_bytes(length)
    |> Base.url_encode64()
    |> binary_part(0, length)
  end

  @doc """
  Get a login cookie tuple with the given key and `insecure` boolean.
  """
  def login_cookie(key, insecure) do
    value =
      case insecure do
        true -> "key=#{key}; Max-Age=63072000"
        false -> "key=#{key}; Secure; HttpOnly; Max-Age=63072000"
      end

    {"set-cookie", value}
  end
end
