defmodule Formal.Host.Pages do
  alias Formal.Web.UI
  def login do
    UI.admin(
      title: "Login to Your Account",
      description:
        "Login to your website builder account to manage your sites or create a new one.",
      body: UI.login([])
    )   
  end 

  def join do
    UI.admin(
      title: "Create a Free Website",
      description: "Create your first website.",
      body: UI.join([])
    )   
  end 

  def new_site do
    UI.admin(
      title: "Create Your Site's Home Page",
      description: "Create a home page for your new site.",
      body: UI.new_site([])
    )   
  end 

  def passphrase(passphrase) do
    UI.admin(
      title: "Your New Studio Account",
      description: "Welcome to your new account.",
      body: UI.passphrase(passphrase: passphrase)
    )   
  end 

  def site(name, pages) do
    UI.admin(
      title: "Welcome to Your Site #{name}",
      description: "Manage your site's pages.",
      body: UI.site(name: name, pages: pages),
      nav: [%{href: "?new-page", text: "Create a new page."}]
    )   
  end 
end
