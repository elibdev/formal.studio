defmodule Formal.Site do
  @moduledoc """
  Manage and operate the databases for individual sites.
  """
  alias Formal.Web
  alias Formal.Site.Pages
  use Web.Service

  @doc """
  Start and link a new `Formal.Site` process. 
  Requires `:path` and optionally `:name` keyword arguments. 
  Registers with `Formal.Registry` under the given name.
  """
  def start_link(opts) when is_list(opts) do
    path = Keyword.fetch!(opts, :path)
    case Keyword.fetch(opts, :name) do
      {:ok, name} -> GenServer.start_link(__MODULE__, path, name: name)
      :error -> GenServer.start_link(__MODULE__, path)
    end
  end

  @impl GenServer
  def init(path) do
    db =
      case File.exists?(path) do
        true -> open(path)
        false -> create(path)
      end

    {:ok, db}
  end

  @impl Web.Service
  def route(:GET, [name], _, _, db) do
    Pages.page(db, name)
  end

  defp create(path, content \\ "") do
    {:ok, db} = GenServer.start_link(Depo.DB, create: path)
    setup(db)
    edit_page(db, "", content)

    # use the default stylesheet
    style = File.read!(Path.join(:code.priv_dir(:formal), "style.css"))
    edit_style(db, style)

    db
  end

  defp open(path) do
    {:ok, db} = GenServer.start_link(Depo.DB, path)
    setup(db)
    db
  end

  defp setup(db) do
    schema = """
    create table if not exists pages (
      name unique not null primary key,
      markdown,
      body,
      title,
      description
    )
    """

    Depo.write(db, schema)

    Depo.teach(db, %{
      edit_page: "insert or replace into pages
      (name, markdown, body, title, description)
      values (?1, ?2, ?3, ?4, ?5)",
      page_markdown: "select markdown from pages where name=?1",
      page: "select body,title,description from pages where name=?1",
      pages: "select name,body,title,description from pages"
    })

    db
  end

  @doc """
  Edit the site's page with the given name using the given markdown text.
  """
  def edit_page(db, name, markdown) do
    body = Cmark.to_html(markdown, [:smart, :safe])
    title = Web.select(body, "h1")
    description = Web.select(body, "p")

    Depo.write(db, :edit_page, [name, markdown, body, title, description])
  end

  @doc """
  Get the page with the given name of the given site. 

  Returns a map with keys `:body`, `:title`, `:description`, and `:markdown`.
  """
  def page(db, name) do
    [page] = Depo.read(db, :page, name)
    page
  end

  @doc """
  Get the CSS stylesheet of the given site. 
  """
  def style(db) do
    [%{body: style}] = Depo.read(db, :page, "style.css")
    style
  end

  @doc """
  Edit the CSS stylesheet of the given site. 
  """
  def edit_style(db, style) do
    Depo.write(db, :edit_page, ["style.css", "", style, "", ""])
  end

  @doc """
  Get the markdown content of the page with the given name.
  """
  def page_markdown(db, name) do
    [%{markdown: markdown}] = Depo.read(db, :page_markdown, name)
    markdown
  end

  @doc """
  Get all pages of the given site. 

  Returns a list of pages, where each page
  is a map with keys `:body`, `:title`, `:description`.
  """
  def pages(db) do
    for page <- Depo.read(db, :pages), page[:name] != "style.css", do: page
  end
end
