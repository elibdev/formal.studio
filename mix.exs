defmodule Formal.MixProject do
  use Mix.Project

  def project do
    [
      app: :formal,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: true,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Formal.Host, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:elli, "~> 2.0"},
      {:tesla, "~> 1.0"},
      {:floki, "~> 0.20.3"},
      {:comeonin, "~> 4.1"},
      {:cmark, "~> 0.7"},
      {:argon2_elixir, "~> 1.2"},
      {:depo, "~> 1.6"},
      {:passphrase, "~> 0.1.0"},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false}
    ]
  end
end
