defmodule HostTest do
  use ExUnit.Case, async: true
  doctest Formal.Host
  alias Formal.Web

  setup_all [:tmp_dir, :client]

  describe "pages for not-logged in user" do
    test "home page has join form", %{client: c} do
      {200, _, body} = Web.get(c, "")
      assert Web.select(body, "form[method=POST]>input[type=email][name=email]")
      assert Web.select(body, "form[method=POST]>input[type=submit]")
    end

    test "login form has email input", %{client: c} do
      {200, _, body} = Web.get(c, "?login")
      assert Web.select(body, "form[method=POST]>input[type=email][name=email]")
      assert Web.select(body, "form[method=POST]>input[type=password][name=passphrase]")
      assert Web.select(body, "form[method=POST]>input[type=submit]")
    end
  end

  describe "new user flow" do
    test "create an account and new site", %{client: c} do
      email = sample_email()
      {201, headers, body} = Web.post(c, "", %{"email" => email})
      # this page shouldn't be cached by the browser
      assert Enum.all?(["no-cache", "no-store", "must-revalidate"], fn key ->
               Map.has_key?(headers["cache-control"], key)
             end)

      # site creation should create an account and store a key for it as a cookie
      cookie = headers["set-cookie"]
      assert cookie["key"] != nil
      assert Map.has_key?(cookie, "Secure")
      assert Map.has_key?(cookie, "HttpOnly")
      assert cookie["Max-Age"] == "63072000"
      # user gets a passphrase to sign in to their account
      assert Web.select(body, ".passphrase")
      # page should include a link to create a new site
      assert Web.select(body, "a[href='/?new-site']")

      auth = [cookie: "key=#{cookie["key"]}"]
      {200, _, body} = Web.get(c, "?new-site", headers: auth)
      assert Web.select(body, "form[method=POST]>input[name=name]")
      assert Web.select(body, "form[method=POST]>textarea[name=content]")
      assert Web.select(body, "form[method=POST]>input[type=submit]")
    end
  end

  describe "signed-in user" do
    setup [:new_user, :new_site, :new_page]

    test "new page form exists", %{client: c, auth: auth} do
      {200, _, body} = Web.get(c, "?new-page", headers: auth)
      assert Web.select(body, "form[method=POST]>input[name=name]")
      assert Web.select(body, "form[method=POST]>textarea[name=content]")
      assert Web.select(body, "form[method=POST]>input[type=submit]")
    end

    test "edit home page", %{client: c, auth: auth, site_name: name} do
      {200, _, body} = Web.get(c, name, headers: auth)
      assert Web.select(body, "a[href='?edit']")

      edit = "#{name}?edit"
      {200, _, body} = Web.get(c, edit, headers: auth)
      assert Web.select(body, "form[method=POST]>textarea[name=content]")
      assert Web.select(body, "input[type=submit]")

      body = %{"content" => sample_content()}
      {302, headers, _} = Web.post(c, edit, body, headers: auth)
      assert Map.has_key?(headers["location"], "/#{name}")
    end

    test "edit existing page", %{client: c, auth: auth, site_name: name} = context do
      [{{:page, page_name}, _}] =
        Enum.filter(context, fn
          {{:page, _}, _} -> true
          _ -> false
        end)

      page_url = "#{name}/#{page_name}"
      {200, _, body} = Web.get(c, page_url, headers: auth)
      assert Web.select(body, "a[href='?edit']")

      edit = "#{page_url}?edit"
      {200, _, body} = Web.get(c, edit, headers: auth)
      assert Web.select(body, "form[method=POST]>textarea[name=content]")
      assert Web.select(body, "input[type=submit]")

      body = %{"name" => sample_site_name(), "content" => sample_content()}
      {302, headers, _} = Web.post(c, edit, body, headers: auth)
      assert Map.has_key?(headers["location"], "/#{page_url}")
    end

    test "create additional page", %{client: c, auth: auth, site_name: name} do
      page = %{
        name: sample_site_name(),
        content: "# #{sample_title()}\n\n#{sample_description()}"
      }

      {302, headers, _} = Web.post(c, "?new-page", page, headers: auth)
      assert Map.has_key?(headers["location"], "/#{name}/#{page[:name]}")

      {200, _, body} = Web.get(c, "", headers: auth)
      assert String.contains?(body, page[:name])
    end

    test "edit stylesheet", %{client: c, auth: auth, site_name: name} do
      url = "#{name}/style.css"
      edit = "#{url}?edit"
      {200, _, body} = Web.get(c, "", headers: auth)
      assert Web.select(body, "a[href='/#{edit}']")

      {200, _, body} = Web.get(c, edit, headers: auth)
      assert Web.select(body, "form[method=POST]>textarea[name=content]")
      assert Web.select(body, "input[type=submit]")

      style = "body {font-family: sans-serif;}"
      body = %{"content" => style}
      {302, headers, _} = Web.post(c, edit, body, headers: auth)
      assert Map.has_key?(headers["location"], "/")

      {200, _, body} = Web.get(c, name)
      assert Web.select(body, "link[rel=stylesheet][href='/#{url}']")
      {200, headers, body} = Web.get(c, "#{name}/style.css")
      assert body == style
      assert headers["content-type"] == %{"text/css" => nil}
    end

    test "view admin page with first site", %{client: c, site_name: name, auth: auth} do
      # admin page should have a link to the new site's home page
      {200, _, body} = Web.get(c, "", headers: auth)
      assert Web.select(body, "a[href='/#{name}']")
    end

    test "logout from initial session", %{client: c, site_name: name, auth: auth} do
      # the key should be invalid after logging out
      {302, headers, _} = Web.post(c, "?logout", %{}, headers: auth)
      assert Map.has_key?(headers["location"], "/")
      {200, _, body} = Web.get(c, "", headers: auth)
      # the home page should have the join form again when logged out
      assert Web.select(body, "form[method=POST]>input[name=email]")

      # new site should be visible to logged out user
      {200, _, _} = Web.get(c, name)
    end

    test "login with passphrase", %{client: c, email: email, pass: pass} do
      # login and get redirected to home page
      credentials = %{email: email, passphrase: pass}
      {302, headers, _} = Web.post(c, "?login", credentials)
      key = headers["set-cookie"]["key"]
      assert key != nil
      assert headers["location"] == %{"/" => nil}
      auth = [cookie: "other=bogus; key=#{key}"]

      # home page should have a logout button when logged in
      {200, _, body} = Web.get(c, "", headers: auth)
      assert Web.select(body, "form[method=POST][action='/?logout']>input[type=submit]")
    end
  end

  defp tmp_dir(_context) do
    # use a random string part to prevent parallel test interference
    dir = Path.join(System.tmp_dir(), "studio-#{Web.random_string(12)}")
    File.mkdir!(dir)

    on_exit(fn ->
      File.rm_rf!(dir)
    end)

    # make the application use our temp dir
    Application.stop(:formal)
    {:ok, _} = Formal.Host.start(:temporary, dir: dir)

    %{dir: dir}
  end

  defp client(_context) do
    client = Web.client(8000)

    %{client: client}
  end

  defp new_site(%{client: c, auth: auth}) do
    body = %{
      name: sample_site_name(),
      content: "# #{sample_title()}\n\n#{sample_description()}"
    }

    {302, headers, _} = Web.post(c, "?new-site", body, headers: auth)
    assert Map.has_key?(headers["location"], "/#{body[:name]}")

    %{site_name: body[:name], home_page: body[:content]}
  end

  defp new_page(%{client: c, auth: auth, site_name: name}) do
    page = %{
      name: sample_site_name(),
      content: sample_content()
    }

    {302, headers, _} = Web.post(c, "?new-page", page, headers: auth)
    assert Map.has_key?(headers["location"], "/#{name}/#{page[:name]}")

    %{{:page, page[:name]} => page[:content]}
  end

  defp sample_site_name() do
    Passphrase.new(3)
    |> String.replace(" ", "-")
  end

  defp sample_title() do
    Passphrase.new(5)
    |> String.split()
    |> Enum.map(&String.capitalize/1)
    |> Enum.join(" ")
  end

  defp sample_description() do
    String.capitalize(Passphrase.new(9) <> ".")
  end

  defp sample_content() do
    "# #{sample_title()}\n\n#{sample_description()}"
  end

  defp sample_email() do
    String.replace(Passphrase.new(2), " ", ".") <> "@example.com"
  end

  defp new_user(%{client: c}) do
    email = sample_email()
    {201, headers, body} = Web.post(c, "", %{"email" => email})
    key = headers["set-cookie"]["key"]
    pass = Web.select(body, ".passphrase")
    auth = [cookie: "other=bogus; key=#{key}"]

    %{auth: auth, pass: pass, email: email}
  end
end
