use Mix.Config

# make password hashing less secure on tests
if Mix.env() == :test do
  config :argon2_elixir, t_cost: 1, m_cost: 8
end
