
# Changelog

This document describes the changes made to each version of Formal Studio.

## v0.2 (Unreleased)

- signed-in user can download full site export
- user can import a previously exported site
- add [TLS][tls] and automatic certificate management via [Acme][acme]
- add configurable account limits
- use the first image on a page for its meta cover image
- user can choose between several themes when creating or editing a site
- package is published on [hex.pm](https://hex.pm)
- add Vue.js packaged in for easy scripting in page content or templates
- automatically generate meta info for SEO and sharing
- admin user can view all sites
- add preview passphrase for an unpublished site
- users can invite other users to have edit or admin permissions for a site
- refactor from using `Depo` to have a `DB` wrapper around [RocksDB][rocks]
that automatically encodes anything but bytes with [MessagePack][msg]

[tls]: https://hex.pm/packages/fast_tls
[acme]: https://hex.pm/packages/acme
[rocks]: https://hex.pm/packages/rocksdb
[msg]: https://hex.pm/packages/msgpax

## v0.1 (Unreleased)

### To Do

- make pages private by default
- add button to publish (and choose site and page name) or unpublish page
- change css to use a responsive grid layout consistent across interface
- write markdown how-to pages
- add copy-to-clipboard button for passphrase
- change language to be consistent across interface about each account having one website
- make error messages more human friendly

### In Progress

- factor out `Site.Supervisor` from `Host`
- make menu on the editing interface usable
- add help pages for using Markdown to make basic page elements and [CSS][css]
- add tap to copy to passphrase

[css]: https://developer.mozilla.org/en-US/docs/Web/CSS

### Added

- refactor `Studio` into `Formal.Studio` and extract `Formal.Web` generic functionality
- add a link to site home using the home page's title on every other page
- add ability for users to edit their stylesheet
- store all of a user's pages in one database
- make user choose site name in initial sign-up
- change page paths to use subpaths of the site name
- change join flow to have user create home page after creating account
- show page title and description along with name on admin page
- add a configurable top nav bar across all pages
- change page view while signed in to include top bar with edit button
- change edit page to full page textarea with top bar with publish button
- change user onboarding to 
`enter email -> get passphrase -> create new site`
- add markdown help page with link in admin (link to <http://commonmark.org/help/>)
- signed-in user can create new sites
- remove any unsafe HTML from the input markdown
- signed-in user can edit markdown landing pages
- new site form generates a markdown page
- improve joining flow to 
`enter title, description, email -> get passphrase -> admin`
- refactor tests to make them more readable using `describe`
- rename `Studio.Page` to `Studio.UI` to more accurately describe it
- factor out request handling from `Studio` into `Studio.Handler`
- factor out database operations into `Studio.Host` and `Studio.Site` modules
- rename project from `Site` to `Studio`
- modernize look
- factor out EEx templates into `/priv` directory
- factor out CSS into `/priv` directory
- signed-in user can view their existing sites
- home page has form for creating a new site (name, title, description, email)
- sites are read from SQLite files in a directory
- each site is served at its own path (the site's name)
- each site's data is stored in its own SQLite database
- after entering site details, user gets a link for sharing
- signed-in user can create a new site
- add ability to log out
- add basic styling for the pages
- home page has join form and a link to login page
- user can sign up for an account with email address
- user is given a secure passphrase once they create their account
- user can sign in to existing account with email and passphrase
- include installation and development documentation in README and Hex Docs

